﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MyProjectNetCore.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blog> Blog { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<MyOwnTableModel> MyOwnTbl {get;set; }
    }

    public partial class MyOwnTableModel
    {
        [Key]
        public int MyOwnId { get; set; }
        public string MyField { get; set; }
    }

    public partial class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

    }

    public partial class Post
    {
        public int PostId { get; set; }
        public int BlogId { get; set; }
        public string Content { get; set; }
        public string Title { get; set; }
    }

}
